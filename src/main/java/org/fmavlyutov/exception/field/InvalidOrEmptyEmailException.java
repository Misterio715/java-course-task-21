package org.fmavlyutov.exception.field;

public final class InvalidOrEmptyEmailException extends AbstractFieldException {

    public InvalidOrEmptyEmailException() {
        super("Invalid or empty email!");
    }

}
