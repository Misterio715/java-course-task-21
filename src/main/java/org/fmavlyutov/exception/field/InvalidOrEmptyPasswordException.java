package org.fmavlyutov.exception.field;

public final class InvalidOrEmptyPasswordException extends AbstractFieldException {

    public InvalidOrEmptyPasswordException() {
        super("Invalid or empty password!");
    }

}
