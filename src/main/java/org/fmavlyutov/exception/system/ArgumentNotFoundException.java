package org.fmavlyutov.exception.system;

public final class ArgumentNotFoundException extends AbstractSystemException {

    public ArgumentNotFoundException(String message) {
        super(message);
    }

}
