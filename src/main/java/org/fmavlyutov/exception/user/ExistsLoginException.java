package org.fmavlyutov.exception.user;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Login already exists!");
    }

}
