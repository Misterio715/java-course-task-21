package org.fmavlyutov.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("No permission!");
    }

}
