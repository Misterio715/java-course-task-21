package org.fmavlyutov.command.system;

public final class SystemExitCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "finish program";
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
