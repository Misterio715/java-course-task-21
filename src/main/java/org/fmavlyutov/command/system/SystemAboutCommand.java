package org.fmavlyutov.command.system;

public final class SystemAboutCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "display info about author";
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("Author: Philip Mavlyutov\n");
    }
}
