package org.fmavlyutov.command.task;

import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "show task by id";
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(getUserId(), id);
        showTask(task);
    }

}
