package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "start task by id";
    }

    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().changeStatusById(getUserId(), id, Status.IN_PROGRESS);
        showTask(task);
    }

}
