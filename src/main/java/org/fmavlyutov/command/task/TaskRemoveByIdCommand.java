package org.fmavlyutov.command.task;

import org.fmavlyutov.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "delete task by id";
    }

    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(getUserId(), id);
    }

}
