package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public String getDescription() {
        return "logout user";
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT USER]");
        getAuthService().logout();
    }

}
