package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "start project by index";
    }

    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Project project = getProjectService().changeStatusByIndex(getUserId(), index, Status.IN_PROGRESS);
        showProject(project);
    }

}
