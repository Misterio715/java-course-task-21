package org.fmavlyutov.api.service;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name, String description) throws InvalidOrEmptyNameException;

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

}
