package org.fmavlyutov.api.service;

import org.fmavlyutov.api.repository.IUserOwnedRepository;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    void clear(String userId);

    Integer getSize(String userId);

    Boolean existsById(String userId, String id);

}
