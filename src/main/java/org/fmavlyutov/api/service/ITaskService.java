package org.fmavlyutov.api.service;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task create(String userId, String name, String description);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    List<Task> findAllByProjectId(String userId, String id);

}
