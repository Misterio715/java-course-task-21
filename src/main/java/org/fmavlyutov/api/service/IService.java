package org.fmavlyutov.api.service;

import org.fmavlyutov.api.repository.IRepository;
import org.fmavlyutov.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
