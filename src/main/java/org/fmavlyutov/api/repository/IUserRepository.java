package org.fmavlyutov.api.repository;

import org.fmavlyutov.model.User;

public interface IUserRepository extends IRepository<User> {

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    Boolean isLoginExists(String login);

    Boolean isEmailExists(String email);

}
