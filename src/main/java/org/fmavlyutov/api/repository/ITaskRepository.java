package org.fmavlyutov.api.repository;

import org.fmavlyutov.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String id);

}
