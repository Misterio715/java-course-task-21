package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.ICommandRepository;
import org.fmavlyutov.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) {
            return;
        }
        final String name = command.getName();
        if (name != null && !name.isEmpty()) {
            mapByName.put(name, command);
        }
        final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) {
            mapByArgument.put(argument, command);
        }
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        return mapByArgument.getOrDefault(argument, null);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return mapByName.getOrDefault(name, null);
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

}
