package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IUserRepository;
import org.fmavlyutov.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOneByLogin(final String login) {
        return records.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return records.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExists(final String login) {
        return records.stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public Boolean isEmailExists(final String email) {
        return records.stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
