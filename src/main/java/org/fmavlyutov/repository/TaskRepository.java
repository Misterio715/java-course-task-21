package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String id) {
        if (userId == null) {
            return Collections.emptyList();
        }
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
