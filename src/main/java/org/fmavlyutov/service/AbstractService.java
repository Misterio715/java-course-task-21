package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IRepository;
import org.fmavlyutov.api.service.IService;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.exception.entity.EmptyModelException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) {
            throw new EmptyModelException();
        }
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) {
            return findAll();
        }
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) {
            return findAll();
        }
        return repository.findAll(sort.getComparator());
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return repository.findOneByIndex(index);
    }

    @Override
    public M remove(final M model) {
        if (model == null) {
            throw new EmptyModelException();
        }
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return repository.removeByIndex(index);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    public Boolean existsById(final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.existsById(id);
    }

}
