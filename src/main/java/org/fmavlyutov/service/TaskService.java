package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.entity.TaskNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.exception.user.EmptyUserIdException;
import org.fmavlyutov.model.Task;

import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = new Task();
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        return add(userId, task);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = repository.findOneById(userId, id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        final Task task = repository.findOneByIndex(userId, index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final Task task = repository.findOneById(userId, id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        final Task task = repository.findOneByIndex(userId, index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            return Collections.emptyList();
        }
        return repository.findAllByProjectId(userId, id);
    }

}
