package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.IProjectTaskService;
import org.fmavlyutov.exception.entity.ProjectNotFoundException;
import org.fmavlyutov.exception.entity.TaskNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.user.EmptyUserIdException;
import org.fmavlyutov.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new InvalidOrEmptyIdException("Invalid or empty project id!");
        }
        if (taskId == null || taskId.isEmpty()) {
            throw new InvalidOrEmptyIdException("Invalid or empty task id!");
        }
        if (!projectRepository.existsById(userId, projectId)) {
            throw new ProjectNotFoundException();
        }
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new InvalidOrEmptyIdException("Invalid or empty project id!");
        }
        if (taskId == null || taskId.isEmpty()) {
            throw new InvalidOrEmptyIdException("Invalid or empty task id!");
        }
        if (!projectRepository.existsById(userId, projectId)) {
            throw new ProjectNotFoundException();
        }
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null ) {
            throw new TaskNotFoundException();
        }
        task.setProjectId(null);;
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (projectId == null || projectId.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) {
            taskRepository.removeById(userId, task.getId());
        }
        projectRepository.removeById(userId, projectId);
    }

}
